const express = require('express');
const router = new express.Router();
const { asyncWrapper: aW } = require('./helpers/asyncWrapper');
const { authMiddleware } = require('../routers/middlewares/authMiddleware');
const {
  dbGetTrucks,
  dbAddTruck,
  dbGetTruckById,
  dbUpdTruckById,
  dbDelTruckById,
  dbAssignTruck
} = require('../controllers/trucksController');

router.get('/', aW(authMiddleware), aW(dbGetTrucks));
router.post('/', aW(authMiddleware), aW(dbAddTruck));
router.get('/:id', aW(authMiddleware), aW(dbGetTruckById));
router.put('/:id', aW(authMiddleware), aW(dbUpdTruckById));
router.delete('/:id', aW(authMiddleware), aW(dbDelTruckById));
router.post('/:id/assign', aW(authMiddleware), aW(dbAssignTruck));

module.exports = router;
