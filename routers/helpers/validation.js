const Joi = require('joi');

const options = {
  errors: {
    wrap: {
      label: "'"
    }
  }
};

const validateAgainstSchema = async (schema, object) =>
  schema.validateAsync(object, options);

const userSchema = Joi.object().keys({
  email: Joi.string().email().required(),
  role: Joi.string().pattern(new RegExp(/^(SHIPPER|DRIVER)$/s)),
  password: Joi.string().alphanum().min(3).max(30)
});

const truckSchema = Joi.object().keys({
  type: Joi.string().pattern(
    new RegExp(/^(SPRINTER|SMALL STRAIGHT|LARGE STRAIGHT)$/s)
  )
});

const skipLimitParamsSchema = Joi.object().keys({
  skip: Joi.number().min(0),
  limit: Joi.number().min(0)
});

const loadSchema = Joi.object().keys({
  name: Joi.string(),
  payload: Joi.number().min(0),
  pickup_address: Joi.string(),
  delivery_address: Joi.string(),
  dimensions: Joi.object().keys({
    width: Joi.number().min(0),
    length: Joi.number().min(0),
    height: Joi.number().min(0)
  })
});

module.exports.validateUser = async obj =>
  await validateAgainstSchema(userSchema, obj);

module.exports.validateTruck = async obj =>
  await validateAgainstSchema(truckSchema, obj);

module.exports.validateSkipLimit = async obj =>
  await validateAgainstSchema(skipLimitParamsSchema, obj);

module.exports.validateLoad = async obj =>
  await validateAgainstSchema(loadSchema, obj);
