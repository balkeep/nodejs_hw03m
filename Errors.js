module.exports.InvalidTokenError = class InvalidTokenError extends Error {
  /**
   * An Error to be thrown when the token is invalid
   * @param {String} message Message containing Error specifics
   */
  constructor(message = 'Invalid JWT token!') {
    super(message);
    this.statusCode = 401;
  }
};

module.exports.BadPasswordError = class BadPasswordError extends Error {
  /**
   * An Error to be thrown when wrong or wrongly formatted password is provided
   * @param {String} message Message containing Error specifics
   */
  constructor(message = 'Wrong password!') {
    super(message);
    this.statusCode = 400;
  }
};

module.exports.NoUserError = class NoUserError extends Error {
  /**
   * An Error to be thrown when request contains wrong user data
   * @param {String} message Message containing Error specifics
   */
  constructor(message = 'User does not exist!') {
    super(message);
    this.statusCode = 400;
  }
};

module.exports.NoTokenError = class NoTokenError extends Error {
  /**
   * An Error to be thrown when the token is not provided with the request
   * @param {String} message Message containing Error specifics
   */
  constructor(message = 'Wrong or no JWT token found!') {
    super(message);
    this.statusCode = 401;
  }
};

module.exports.WrongEntityIdError = class WrongEntityIdError extends Error {
  /**
   * An Error to be thrown when incorrect truck id is provided
   * @param {String} message Message containing Error specifics
   */
  constructor(message = 'Wrong Entity ID!') {
    super(message);
    this.statusCode = 400;
  }
};

module.exports.NotYoursError = class NotYoursError extends Error {
  /**
   * An Error to be thrown when user tries to access trucks/loads/etc... that
   * does not belong to him
   * @param {String} message Message containing Error specifics
   */
  constructor(
    message = 'You are asking for something that does not belong to you!'
  ) {
    super(message);
    this.statusCode = 401;
  }
};

module.exports.NonExistantTypeError = class NonExistantTypeError extends Error {
  /**
   * An Error to be thrown when user tries to access item with wrong Item Type
   * @param {String} message Message containing Error specifics
   */
  constructor(message = 'Check your types') {
    super(message);
    this.statusCode = 400;
  }
};

module.exports.NoTruckAssignedError = class NoTruckAssignedError extends Error {
  /**
   * An Error to be thrown when user wants to get shipping info on a load with
   * no truck assigned
   * @param {String} message Message containing Error specifics
   */
  constructor(message = 'This load has no truck assigned to it!') {
    super(message);
    this.statusCode = 400;
  }
};

module.exports.UnfittableLoadError = class UnfittableLoadError extends Error {
  /**
   * An Error to be thrown when user wants to post a load that we have no
   * trucks of suitable capacity for
   * @param {String} message Message containing Error specifics
   */
  constructor(message = 'This load cannot fit in any truck') {
    super(message);
    this.statusCode = 400;
  }
};
module.exports.OnlyPostingNewLoadsError = class OnlyPostingNewLoadsError extends (
  Error
) {
  /**
   * An Error to be thrown when user wants to post a load that is not new
   * @param {String} message Message containing Error specifics
   */
  constructor(message = 'Can only post loads with the status NEW') {
    super(message);
    this.statusCode = 400;
  }
};

module.exports.NoActiveLoadError = class NoActiveLoadError extends Error {
  /**
   * An Error to be thrown when user has no active loads and tries to progress
   * one
   * @param {String} message Message containing Error specifics
   */
  constructor(message = 'You have no active loads') {
    super(message);
    this.statusCode = 400;
  }
};

module.exports.LoadArchivedError = class LoadArchivedError extends Error {
  /**
   * An Error to be thrown when user tries to progressarchived load
   * @param {String} message Message containing Error specifics
   */
  constructor(message = 'Load is already SHIPPED and cannot be modified') {
    super(message);
    this.statusCode = 400;
  }
};
